﻿using GDLibrary;
using GDLibrary.Actors.Camera;
using GDLibrary.Actors.Drawn._2D.UI;
using GDLibrary.Actors.Drawn._2D.HUD;
using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Actors.Drawn2D.UI;
using GDLibrary.Controller._2D.Base;
using GDLibrary.Controller.Base;
using GDLibrary.Controller.Camera3D;
using GDLibrary.Controllers.Base;
using GDLibrary.Controllers.Camera3D;
using GDLibrary.Controllers.Camera3D.Collidable;
using GDLibrary.Curve;
using GDLibrary.Debug;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.GDDebug.Physics;
using GDLibrary.Interfaces;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Content;
using GDLibrary.Managers.Input;
using GDLibrary.Managers.Object;
using GDLibrary.Managers.Physics;
using GDLibrary.Managers.Screen;
using GDLibrary.Managers.UI;
using GDLibrary.Parameters.Camera;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Utility;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNAIntroClass.App.Actors;
using XNAIntroClass.Data;
using XNAIntroClass.Menu;
using GDLibrary.Managers.Sound;

namespace XNAIntroClass
{
    public class Main : Microsoft.Xna.Framework.Game
    {
        #region Fields
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public ObjectManager objectManager { get; private set; }
        public CameraManager cameraManager { get; private set; }
        public MouseManager mouseManager { get; private set; }
        public KeyboardManager keyboardManager { get; private set; }
        public ScreenManager screenManager { get; private set; }
        public MyAppMenuManager menuManager { get; private set; }
        public PhysicsManager physicsManager { get; private set; }
        public UIManager uiManager { get; private set; }

        public SoundManager soundManager { get; private set; }

        //receives, handles and routes events
        public EventDispatcher eventDispatcher { get; private set; }

        private BasicEffect modelEffect;
        private BasicEffect unlitModelEffect;

        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;

        //stores curves and rails used by cameras
        private Dictionary<string, Transform3DCurve> curveDictionary;
        private Dictionary<string, RailParameters> railDictionary;
        //stores viewport layouts for multi-screen layout
        private Dictionary<string, Viewport> viewPortDictionary;

        //private ModelObject drivableBoxObject;

        private HUD hud;
        private DebugDrawer debugDrawer;
        private PhysicsDebugDrawer physicsDebugDrawer;

        private CollidableObject collidableBall;
        private bool ballHasStopped = false;

        private int stroke = 0;
        private float angle = 0.0f;
        private float multiplier = 20.0f;

        private Vector3 ball_last_static_position = new Vector3(20, -17, 5);

        #endregion

        #region Constructor
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            int gameLevel = 1;
            bool isMouseVisible = true;
            Integer2 screenResolution = ScreenUtility.HD720;
            ScreenUtility.ScreenType screenType = ScreenUtility.ScreenType.SingleScreen;

            //set the title
            Window.Title = "Pitch to Pluto";

            //Initilize Effects
            InitializeEffects();

            //initialize the Dispatcher
            InitializeEventDispatcher();

            //Initialize the managers
            InitializeManagers(screenResolution, screenType, isMouseVisible);

            //Load dictionaries, media assets and non-media assets
            LoadDictionaries();
            LoadAssets();
            LoadViewports(screenResolution);

            //load game happens before cameras are loaded as we may need a third person camera that needs a 
            //reference to a loaded actor
            LoadGame(gameLevel);

            //Initialize cameras based in the desired screen layout
            InitializeSingleScreenCycleableCameraDemo(screenResolution);

            //Publish Game Start event
            StartGame();
            //soundManager.PlayCue("MenuMusic");
            
            #if DEBUG
            InitializeDebugCollisionSkinInfo();
            #endif

            base.Initialize();
        }

        private void InitializeManagers(Integer2 screenResolution, ScreenUtility.ScreenType screenType, bool isMouseVisible)
        {
            this.cameraManager = new CameraManager(this, 1, this.eventDispatcher);
            Components.Add(this.cameraManager);

            this.objectManager = new ObjectManager(this, cameraManager, this.eventDispatcher, 10);
            //Components.Add(this.objectManager);

            this.keyboardManager = new KeyboardManager(this);
            Components.Add(this.keyboardManager);

            //create the manager which supports multiple camera viewports
            this.screenManager = new ScreenManager(this, graphics, screenResolution,
                screenType, this.objectManager, this.cameraManager,
                this.keyboardManager, AppData.KeyPauseShowMenu, this.eventDispatcher, StatusType.Off);
            Components.Add(this.screenManager);

            this.physicsManager = new PhysicsManager(this, this.eventDispatcher, StatusType.Off);
            Components.Add(this.physicsManager);

            this.mouseManager = new MouseManager(this, isMouseVisible, this.physicsManager);
            Components.Add(this.mouseManager);

            this.soundManager = new SoundManager(this, this.eventDispatcher, StatusType.Update, "Content/Assets/Audio/",
               "2DSound.xgs", "WaveBankPluto.xwb", "SoundBankPluto.xsb");
            Components.Add(this.soundManager);
        }

        private void LoadDictionaries()
        {
            //models
            this.modelDictionary = new ContentDictionary<Model>("model dictionary", this.Content);

            //textures
            this.textureDictionary = new ContentDictionary<Texture2D>("texture dictionary", this.Content);

            //fonts
            this.fontDictionary = new ContentDictionary<SpriteFont>("font dictionary", this.Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            this.curveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails
            this.railDictionary = new Dictionary<string, RailParameters>();

            //viewports - used to store different viewports to be applied to multi-screen layouts
            this.viewPortDictionary = new Dictionary<string, Viewport>();
        }

        private void LoadAssets()
        {
            #region Models
            this.modelDictionary.Load("Assets/Models/Platform", "Platform");
            this.modelDictionary.Load("Assets/Models/Golf_Ball", "Ball");
            this.modelDictionary.Load("Assets/Models/plane1", "plane1");
            this.modelDictionary.Load("Assets/Models/Asteroid 1", "Asteroid 1");
            this.modelDictionary.Load("Assets/Models/Asteroid 2", "Asteroid 2");
            #endregion

            #region Textures
            //environment
            this.textureDictionary.Load("Assets/Textures/Props/Crates/white_texture");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/pluto_texture");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/neptune_texture");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/platform_texture_space");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/Asteroid_Texture");

            this.textureDictionary.Load("Assets/Textures/Skybox/back");
            this.textureDictionary.Load("Assets/Textures/Skybox/left");
            this.textureDictionary.Load("Assets/Textures/Skybox/right");
            this.textureDictionary.Load("Assets/Textures/Skybox/sky");
            this.textureDictionary.Load("Assets/Textures/Skybox/front");
            this.textureDictionary.Load("Assets/Textures/Skybox/ground");
            this.textureDictionary.Load("Assets/Textures/checkerboard");

            //menu - buttons
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/ButtonTemplate");

            //menu - backgrounds
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/mainmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/Audio_Menu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/Controls_Menu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/exitmenuwithtrans");
            #endregion

            //ui (or hud) elements
            this.textureDictionary.Load("Assets/Textures/UI/HUD/reticuleDefault");

            #region Fonts
            this.fontDictionary.Load("Assets/Debug/Fonts/debug");
            this.fontDictionary.Load("Assets/Fonts/HUDFont");
            this.fontDictionary.Load("Assets/Fonts/menu");
            this.fontDictionary.Load("Assets/Fonts/mouse");
            #endregion
        }

        private void LoadViewports(Integer2 screenResolution)
        {

            //the full screen viewport with optional padding
            int leftPadding = 0, topPadding = 0, rightPadding = 0, bottomPadding = 0;
            Viewport paddedFullViewPort = ScreenUtility.Pad(new Viewport(0, 0, screenResolution.X, (int)(screenResolution.Y)), leftPadding, topPadding, rightPadding, bottomPadding);
            this.viewPortDictionary.Add("full viewport", paddedFullViewPort);

            //work out the dimensions of the small camera views along the left hand side of the screen
            int smallViewPortHeight = 144; //6 small cameras along the left hand side of the main camera view i.e. total height / 5 = 720 / 5 = 144
            int smallViewPortWidth = 5 * smallViewPortHeight / 3; //we should try to maintain same ProjectionParameters aspect ratio for small cameras as the large     
            //the five side viewports in multi-screen mode
            this.viewPortDictionary.Add("column0 row0", new Viewport(0, 0, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row1", new Viewport(0, 1 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row2", new Viewport(0, 2 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row3", new Viewport(0, 3 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row4", new Viewport(0, 4 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            //the larger view to the right in column 1
            this.viewPortDictionary.Add("column1 row0", new Viewport(smallViewPortWidth, 0, screenResolution.X - smallViewPortWidth, screenResolution.Y));
        }

        private void InitializeHud()
        {
            this.hud = new HUD(this, this.screenManager, this.cameraManager, this.objectManager, spriteBatch,
                this.fontDictionary["HUDFont"], Color.GhostWhite, new Vector2(5, 640), this.eventDispatcher, StatusType.Off, this.stroke, this.angle, this.multiplier);
            Components.Add(this.hud);
        }

        #if DEBUG
        private void InitializeDebugTextInfo()
        {
            //add debug info in top left hand corner of the screen
            this.debugDrawer = new DebugDrawer(this, this.screenManager, this.cameraManager, this.objectManager, spriteBatch,
                this.fontDictionary["debug"], Color.White, new Vector2(5, 5), this.eventDispatcher, StatusType.Off);
            Components.Add(this.debugDrawer);

        }

        private void InitializeDebugCollisionSkinInfo()
        {
            //show the collision skins
            this.physicsDebugDrawer = new PhysicsDebugDrawer(this, this.cameraManager, this.objectManager,
                this.screenManager, this.eventDispatcher, StatusType.Off);
            Components.Add(this.physicsDebugDrawer);
        }
        #endif
        #endregion

        #region Load Game Content
        //load the contents for the level specified
        private void LoadGame(int level)
        {
            int worldScale = 250;

            InitializeNonCollidableSkyBox(worldScale);

            InitializeCollidableGround(worldScale);
            InitializeStaticCollidableTriangleMeshObjects();

            Vector3 startingPlatform = new Vector3(20, -17, 5);
            InitializeDynamicCollidableObjects(startingPlatform);
        }

        //skybox is a non-collidable series of ModelObjects with no lighting
        private void InitializeNonCollidableSkyBox(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the skybox decorator elements (e.g. ground, front, top etc). 
            Transform3D transform = new Transform3D(new Vector3(0, -5, 0), new Vector3(worldScale, 1, worldScale));

            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, this.unlitModelEffect, ColorParameters.WhiteOpaque,
                this.textureDictionary["white_texture"], this.modelDictionary["plane1"]);

            //will be re-used for all planes
            ModelObject clonePlane = null;

            #region Skybox
            //add the back skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.Texture = this.textureDictionary["back"];
            //rotate the default plane 90 degrees around the X-axis (use the thumb and curled fingers of your right hand to determine +ve or -ve rotation value)
            clonePlane.Transform3D.Rotation = new Vector3(90, 0, 0);
            /*
             * Move the plane back to meet with the back edge of the grass (by based on the original 3DS Max model scale)
             * Note:
             * - the interaction between 3DS Max and XNA units which result in the scale factor used below (i.e. 1 x 2.54 x worldScale)/2
             * - that I move the plane down a little on the Y-axiz, purely for aesthetic purposes
             */
            clonePlane.Transform3D.Translation = new Vector3(0, -5, (-2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);

            //As an exercise the student should add the remaining 4 skybox planes here by repeating the clone, texture assignment, rotation, and translation steps above...
            //add the left skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.Texture = this.textureDictionary["left"];
            clonePlane.Transform3D.Rotation = new Vector3(90, 90, 0);
            clonePlane.Transform3D.Translation = new Vector3((-2.54f * worldScale) / 2.0f, -5, 0);
            this.objectManager.Add(clonePlane);

            //add the right skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.Texture = this.textureDictionary["right"];
            clonePlane.Transform3D.Rotation = new Vector3(90, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3((2.54f * worldScale) / 2.0f, -5, 0);
            this.objectManager.Add(clonePlane);

            //add the top skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.Texture = this.textureDictionary["sky"];
            //notice the combination of rotations to correctly align the sky texture with the sides
            clonePlane.Transform3D.Rotation = new Vector3(180, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3(0, ((2.54f * worldScale) / 2.0f) - 5, 0);
            this.objectManager.Add(clonePlane);

            //add the front skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.Texture = this.textureDictionary["front"];
            clonePlane.Transform3D.Rotation = new Vector3(-90, 0, 180);
            clonePlane.Transform3D.Translation = new Vector3(0, -5, (2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);

            //add the bottom skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.Texture = this.textureDictionary["ground"];
            clonePlane.Transform3D.Rotation = new Vector3(0, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3(0, -5 - (2.54f * worldScale) / 2.0f, 0);
            this.objectManager.Add(clonePlane);
            #endregion
        }

        private void InitializeCollidableGround(int worldScale)
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            Texture2D texture = null;

            Model model = this.modelDictionary["Platform"];
            texture = this.textureDictionary["platform_texture_space"];
            transform3D = new Transform3D(new Vector3(0, -400, 0), new Vector3(0, 0, 0), new Vector3(100, 0.001f, 100), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new CollidableObject("ground1", ActorType.CollidableGround, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model);
            collidableObject.AddPrimitive(new JigLibX.Geometry.Plane(transform3D.Up, Vector3.Zero), new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);
        }

        //Triangle mesh objects wrap a tight collision surface around complex shapes - the downside is that TriangleMeshObjects CANNOT be moved
        private void InitializeStaticCollidableTriangleMeshObjects()
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            Texture2D texture = null;

            Model model = this.modelDictionary["Platform"];
            texture = this.textureDictionary["platform_texture_space"];
            transform3D = new Transform3D(new Vector3(20, -20, 5), new Vector3(0, 0, 0), new Vector3(0.15f, 0.03f, 0.15f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("platform1", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-50, -10, 0), new Vector3(0, 0, 0), new Vector3(0.15f, 0.03f, 0.15f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("platform2", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-80, 30, 100), new Vector3(0, 0, 0), new Vector3(0.05f, 0.02f, 0.15f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("platform4", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-80, 35, 107), new Vector3(-90, 0, 0), new Vector3(0.05f, 0.02f, 0.15f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("platform4", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-80, 55, 70), new Vector3(90, 0, 0), new Vector3(0.05f, 0.02f, 0.15f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("platform5", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-90, 55, 85), new Vector3(0, 90, 0), new Vector3(0.05f, 0.02f, 0.15f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("platform6", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, texture, model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            Texture2D pluto_texture = this.textureDictionary["pluto_texture"];
            Model ball_model = this.modelDictionary["Ball"];
            transform3D = new Transform3D(new Vector3(-120, 50, -120), new Vector3(0, 0, 0), new Vector3(0.3f, 0.3f, 0.3f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("pluto", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, pluto_texture, ball_model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);

            Texture2D neptune_texture = this.textureDictionary["neptune_texture"];
            transform3D = new Transform3D(new Vector3(120, 0, 0), new Vector3(0, 0, 0), new Vector3(0.6f, 0.6f, 0.6f), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("neptune", ActorType.CollidableProp, transform3D, this.modelEffect, ColorParameters.WhiteOpaque, neptune_texture, ball_model, new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false we can fall through the floor
            this.objectManager.Add(collidableObject);
        }

        private void InitializeDynamicCollidableObjects(Vector3 position)
        {
            //CollidableObject sphereArchetype = null;
            //Transform3D transform3D = null;
            Texture2D texture = null;
            Model model = null;

            #region Spheres
            //these boxes, spheres and cylinders are all centered around (0,0,0) in 3DS Max
            model = this.modelDictionary["Ball"];
            texture = this.textureDictionary["white_texture"];

            this.collidableBall = new CollidableObject("sphere", ActorType.CollidablePickup, Transform3D.Zero, this.modelEffect, ColorParameters.WhiteOpaque, texture, model);
            this.collidableBall.Transform3D = new Transform3D(position, new Vector3(0, 0, 0), new Vector3(0.05f, 0.05f, 0.05f), Vector3.UnitX, Vector3.UnitY);
            this.collidableBall.AddPrimitive(new Sphere(collidableBall.Transform3D.Translation, 0.6f), new MaterialProperties(0.2f, 0.9f, 0.8f));
            this.collidableBall.Enable(false, 10);
            this.objectManager.Add(this.collidableBall);
            #endregion
        }
        #endregion

        #region Initialize Cameras
        private void InitializeCamera(Integer2 screenResolution, string id, Viewport viewPort, Transform3D transform, IController controller)
        {
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardMediumFiveThree, viewPort, 1, StatusType.Update);

            if (controller != null)
                camera.AttachController(controller);

            this.cameraManager.Add(camera);
        }

        private void InitializeSingleScreenCameraDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            string id = "";
            string viewportDictionaryKey = "";

            id = "collidable first person camera";
            viewportDictionaryKey = "full viewport";

            id = "non-collidable third person camera";
            transform = new Transform3D(new Vector3(20, 0, 0), -Vector3.UnitZ, Vector3.UnitY);
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], 1, StatusType.Update);

            camera.AttachController(new ThirdPersonController(
                    camera + " controller",
                    ControllerType.CollidableFirstPerson,
                    this.collidableBall,
                    AppData.CameraThirdPersonDistance,
                    AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                    AppData.CameraThirdPersonElevationAngleInDegrees,
                    AppData.CameraThirdPersonElevationAngleInDegrees,
                    LerpSpeed.VeryFast,
                    LerpSpeed.VeryFast,
                    this.keyboardManager,
                    this.mouseManager,
                    this.cameraManager));

            this.cameraManager.Add(camera);
        }

        private void reAttachCamera()
        {
            string viewportDictionaryKey = "";
            viewportDictionaryKey = "full viewport";
            Transform3D transform = new Transform3D(new Vector3(20, 0, 0), -Vector3.UnitZ, Vector3.UnitY);
            Camera3D camera = new Camera3D("new camera", ActorType.Camera, transform, ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], 1, StatusType.Update);

            camera.AttachController(new ThirdPersonController(
                    camera + " controller",
                    ControllerType.CollidableFirstPerson,
                    this.collidableBall,
                    AppData.CameraThirdPersonDistance,
                    AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                    AppData.CameraThirdPersonElevationAngleInDegrees,
                    AppData.CameraThirdPersonElevationAngleInDegrees,
                    LerpSpeed.VeryFast,
                    LerpSpeed.VeryFast,
                    this.keyboardManager,
                    this.mouseManager,
                    this.cameraManager));

            this.cameraManager.Add(camera);
        }
        private void InitializeSingleScreenCycleableCameraDemo(Integer2 screenResolution)
        {
            InitializeSingleScreenCameraDemo(screenResolution);
        }
        #endregion

        #region Events

        private void InitializeEventDispatcher()
        {
            //initialize based on the expected number of events
            this.eventDispatcher = new EventDispatcher(this, 20);

            //Don't forget to add to the component list, otherwise EventDispatcher::Update will not get called
            Components.Add(this.eventDispatcher);
        }

        private void StartGame()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData("this doesnt matter", this, EventActionType.OnPause, EventCategoryType.MainMenu));

            //publish an event to set the camera
            object[] additionalEventParamsB = { "collidable first person camera 1" };
            EventDispatcher.Publish(new EventData("set camera bla", this, EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
        }
        #endregion

        #region Menu

        private void InitializeMenu()
        {
            this.menuManager = new MyAppMenuManager(this, this.mouseManager, this.keyboardManager, spriteBatch,
                this.eventDispatcher, StatusType.Off);
            //set the main menu to be the active menu scene
            this.menuManager.SetActiveList("mainmenu");
            Components.Add(this.menuManager);
        }

        private void AddMenuElements()
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null, clone = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 50;

            #region Main Menu
            sceneID = "main menu";

            //retrieve the background texture
            texture = this.textureDictionary["mainmenu"];
            //scale the texture to fit the entire screen
            Vector2 scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("mainmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, ColorParameters.WhiteOpaque, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add start button
            buttonID = "startbtn";
            buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 200);
            texture = this.textureDictionary["ButtonTemplate"];
            transform = new Transform2D(position,
                0, new Vector2(1.5f, 0.6f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, new ColorParameters(Color.LightPink, 1), SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);


            //add audio button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "audiobtn";
            clone.Text = "Audio";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add controls button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "controlsbtn";
            clone.Text = "Controls";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "exitbtn";
            clone.Text = "Exit";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightYellow;
            //store the original color since if we modify with a controller and need to reset
            clone.ColorParameters.OriginalColorParameters.Color = clone.ColorParameters.Color;
            //attach another controller on the exit button just to illustrate multi-controller approach
            clone.AttachController(new UIColorSineLerpController("colorSineLerpController", ControllerType.SineColorLerp,
                    new TrigonometricParameters(1, 0.4f, 0), Color.LightSeaGreen, Color.LightGreen));
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Audio Menu
            sceneID = "audio menu";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["Audio_Menu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, ColorParameters.WhiteOpaque, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));


            //add volume up button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "volumeUpbtn";
            clone.Text = "Volume Up";
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightPink;
            this.menuManager.Add(sceneID, clone);

            //add volume down button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            clone.ID = "volumeDownbtn";
            clone.Text = "Volume Down";
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            clone.ID = "volumeMutebtn";
            clone.Text = "Volume Mute";
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Controls Menu
            sceneID = "controls menu";

            //retrieve the controls menu background texture
            texture = this.textureDictionary["Controls_Menu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, ColorParameters.WhiteOpaque, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 9 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            clone.ColorParameters.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion
        }

        private void InitializeUI()
        {
            //holds UI elements (e.g. reticule, inventory, progress)
            this.uiManager = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            Components.Add(this.uiManager);
        }

        private void AddUIElements()
        {
            InitializeUIMousePointer();
            InitializeUIProgress();
            InitializeUIInventoryMenu();
        }

        private void InitializeUIMousePointer()
        {
            Texture2D texture = this.textureDictionary["reticuleDefault"];
            //show complete texture
            Microsoft.Xna.Framework.Rectangle sourceRectangle = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height);

            MyUIMouseObject myUIMouseObject = new MyUIMouseObject("mouseObject",
                ActorType.UIDynamicTexture,
                StatusType.Drawn | StatusType.Update,
                new Transform2D(Vector2.One),
                new ColorParameters(Color.Yellow, 1.0f),
                SpriteEffects.None,
                this.fontDictionary["mouse"],
                "",
                new Vector2(0, -40),
                Color.White,
                1,
                texture,
                sourceRectangle,
                new Vector2(sourceRectangle.Width / 2.0f, sourceRectangle.Height / 2.0f),
                this.mouseManager,
                this.cameraManager,
                AppData.PickStartDistance,
                AppData.PickEndDistance);
            this.uiManager.Add(myUIMouseObject);
        }

        private void InitializeUIProgress()
        {
            //throw new NotImplementedException();
        }

        private void InitializeUIInventoryMenu()
        {
            //throw new NotImplementedException();
        }


        private void InitializeEffects()
        {
            this.modelEffect = new BasicEffect(graphics.GraphicsDevice);
            //enable the use of a texture on a model
            this.modelEffect.TextureEnabled = true;
            //setup the effect to have a single default light source which will be used to calculate N.L and N.H lighting
            this.modelEffect.EnableDefaultLighting();


            //used by the skybox which doesn't respond to lighting within the scene i.e. the skybox is effectively unlit
            this.unlitModelEffect = new BasicEffect(graphics.GraphicsDevice);
            //enable the use of a texture on a model
            this.unlitModelEffect.TextureEnabled = true;
        }
        #endregion

        #region Content, Update, Draw

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            #region Add Menu and UI
            InitializeMenu();
            AddMenuElements();
            InitializeUI();
            InitializeHud();
            AddUIElements();
            #endregion

            #if DEBUG
            InitializeDebugTextInfo();
            #endif

        }

        protected override void UnloadContent()
        {
            // formally call garbage collection to de-allocate resources from RAM
            this.modelDictionary.Dispose();
            this.textureDictionary.Dispose();
            this.fontDictionary.Dispose();
        }

        protected override void Update(GameTime gameTime)
        {
            if (this.collidableBall.Body.Velocity == Vector3.Zero)
            {
                //System.Diagnostics.Debug.WriteLine(this.collidableBall.Body.Velocity);
                this.ballHasStopped = true;
            }

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            playBackgroundMusic();

            #region DEMO
            //only cycle in single screen layout as cycling in multi-screen is meaningless
            if (this.screenManager.ScreenType == ScreenUtility.ScreenType.SingleScreen && this.keyboardManager.IsFirstKeyPress(Keys.C))
            {
                EventDispatcher.Publish(new EventData("set camera please", this, EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }

            //testing event generation on opacity change - see DrawnActor3D::Alpha setter
            if (this.keyboardManager.IsFirstKeyPress(Keys.X))
            {
                float newMultiplier = 22.0f;
                this.multiplier = newMultiplier;
                updateMultiplier(newMultiplier);
                hud.updatePowerLevelOne(newMultiplier);
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.C))
            {
                float newMultiplier = 34.1f;
                this.multiplier = newMultiplier;
                updateMultiplier(newMultiplier);
                hud.updatePowerLevelOne(newMultiplier);
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.V))
            {
                float newMultiplier = 40.0f;
                this.multiplier = newMultiplier;
                updateMultiplier(newMultiplier);
                hud.updatePowerLevelOne(newMultiplier);
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.B))
            {
                float newMultiplier = 35.0f;
                this.multiplier = newMultiplier;
                updateMultiplier(newMultiplier);
                hud.updatePowerLevelOne(newMultiplier);
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.N))
            {
                if(this.multiplier > 0)
                {
                    float newMultiplier = this.multiplier - 1;
                    updateMultiplier(newMultiplier);
                    hud.updatePowerLevelOne(newMultiplier);
                }
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.M))
            {
                if(this.multiplier < 100)
                {
                    float newMultiplier = this.multiplier + 1;
                    updateMultiplier(newMultiplier);
                    hud.updatePowerLevelOne(newMultiplier);
                }
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.Z))
            {
                if (this.ballHasStopped)
                {
                    Vector3 look = cameraManager.ActiveCamera.Transform3D.Look;
                    float shotAngle = 75.0f;
                    float lookX = look.X;
                    float lookY = shotAngle / 90.0f;
                    float lookZ = look.Z;
                    this.collidableBall.Body.Velocity += multiplier * new Vector3(lookX, lookY, lookZ);
                    hud.updateStrokeCount();
                }
            }

            //show/hide debug info
            if (this.keyboardManager.IsFirstKeyPress(Keys.L))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnToggleDebug, EventCategoryType.Debug));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.R))
            {
                collidableBall.Collision.RemoveAllPrimitives();
                this.objectManager.Remove(collidableBall);
                Vector3 secondPlatform = new Vector3(20, -17, 5);
                InitializeDynamicCollidableObjects(secondPlatform);
                cameraManager.clearAllCamera();
                reAttachCamera();
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.T))
            {
                collidableBall.Collision.RemoveAllPrimitives();
                this.objectManager.Remove(collidableBall);
                Vector3 thirdPlatform = new Vector3(-80, 40, 100);
                InitializeDynamicCollidableObjects(thirdPlatform);
                cameraManager.clearAllCamera();
                reAttachCamera();
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.Y))
            {
                collidableBall.Collision.RemoveAllPrimitives();
                this.objectManager.Remove(collidableBall);
                Vector3 thirdPlatform = new Vector3(-90, 65, 85);
                InitializeDynamicCollidableObjects(thirdPlatform);
                cameraManager.clearAllCamera();
                reAttachCamera();
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.U))
            {
                collidableBall.Collision.RemoveAllPrimitives();
                this.objectManager.Remove(collidableBall);
                Vector3 thirdPlatform = new Vector3(-120, 55, -120);
                InitializeDynamicCollidableObjects(thirdPlatform);
                cameraManager.clearAllCamera();
                reAttachCamera();
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
                hud.updateWin();
                demoHudChange();
            }

            #endregion
            demoCameraChange();
            soundManagerTriggers();
            base.Update(gameTime);
        }

        private void addStrokeCount()
        {
            this.stroke++;
        }

        private void updateMultiplier(float m)
        {
            this.multiplier = m;
        }

        private void demoCameraChange()
        {
            //only single in single screen layout since cycling in multi-screen is meaningless
            if (this.screenManager.ScreenType == ScreenUtility.ScreenType.SingleScreen && this.keyboardManager.IsFirstKeyPress(Keys.A))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }
        }

        private void demoHudChange()
        {
             EventDispatcher.Publish(new EventData(EventActionType.OnToggleHud, EventCategoryType.Camera));
        }

        private void playBackgroundMusic()
        {
            soundManager.PlayCue("MenuMusic");
        }

        private void soundManagerTriggers()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.Z))
            {
                soundManager.PlayCue("AngleSelect2");
            }
            if (menuManager.SetActiveList("mainmenu")) //Start when Menu is Active
            {
                soundManager.PlayCue("MenuMusic");
            }
            //if (this.keyboardManager.IsFirstKeyPress(Keys.Q)) //Start when Menu is Active
            //{
            //    soundManager.PlayCue("MenuMusic");
            //}
            if (this.keyboardManager.IsFirstKeyPress(Keys.Space))
            {
                soundManager.PlayCue("MenuClickBackButton");
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.F)) //Manually start Music
            {
                soundManager.ResumeCue("MenuMusic");
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.G)) //Ball Hits Pluto
            {
                soundManager.PlayCue("HoleShots");
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.H)) //Ball hitss on Collidable
            {
                soundManager.PlayCue("BallLand2");
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.NumPad5)) // Pause Sounds
            {
                soundManager.PauseAll();
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.Subtract)) // Increase Volume
            {
                soundManager.ChangeVolume(10, "diegetic");
                soundManager.ChangeVolume(10, "non-diegetic");
            }
            if (this.keyboardManager.IsFirstKeyPress(Keys.Add)) //Decrease Volume
            {
                soundManager.ChangeVolume(-10, "diegetic");
                soundManager.ChangeVolume(-10, "non-diegetic");
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
        }
        #endregion
    }
}