﻿/* Store common hard-coded values within the game
 * e.g key mappings, mouse sensitivity etc*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAIntroClass.Data
{
    public sealed class LerpSpeed
    {
        private static readonly float SpeedMultiplier = 2;
        public static readonly float VerySlow = 0.05f;
        public static readonly float Slow = SpeedMultiplier * VerySlow;
        public static readonly float Medium = SpeedMultiplier * Slow;
        public static readonly float Fast = SpeedMultiplier * Medium;
        public static readonly float VeryFast = SpeedMultiplier * Fast;
    }

    public class AppData
    {
        #region Common
        public static readonly int IndexMoveForward = 0;
        public static readonly int IndexMoveBackward = 1;
        public static readonly int IndexMoveLeft = 2;
        public static readonly int IndexMoveRight = 3;
        public static readonly int IndexMoveJump = 4;
        public static readonly int IndexMoveCrouch = 5;
        public static readonly int IndexStrafeLeft = 6;
        public static readonly int IndexStrafeRight = 7;
        #endregion

        #region Camera
        public static readonly float CameraRotationSpeed = 0.005f;
        public static readonly float CameraMoveSpeed = 0.05f;
        public static readonly float CameraStrafeSpeed = 0.6f * CameraMoveSpeed;

        //JigLib related collidable camera properties
        public static readonly float CollidableCameraJumpHeight = 12;
        public static readonly float CollidableCameraMoveSpeed = 0.6f;
        public static readonly float CollidableCameraStrafeSpeed = 0.6f * CollidableCameraMoveSpeed;
        public static readonly float CollidableCameraCapsuleRadius = 2;
        public static readonly float CollidableCameraViewHeight = 5; //how tall is the first person player?
        public static readonly float CollidableCameraMass = 10;


        public static readonly Keys[] CameraMoveKeys = { Keys.W, Keys.S, Keys.A, Keys.D, Keys.Space, Keys.C };

        public static readonly float CameraThirdPersonScrollSpeedDistanceMultiplier = 0.00125f;
        public static readonly float CameraThirdPersonScrollSpeedElevatationMultiplier = 0.1f;
        public static readonly float CameraThirdPersonDistance = 20;
        public static readonly float CameraThirdPersonElevationAngleInDegrees = 160;

        #endregion

        #region Security Camera
        public static readonly float SecurityCameraRotationSpeedSlow = 0.5f;
        public static readonly float SecurityCameraRotationSpeedMedium = 
            2* SecurityCameraRotationSpeedSlow;
        public static readonly float SecurityCameraRotationSpeedFast =
            2 * SecurityCameraRotationSpeedMedium;

        public static readonly Vector3 SecurityCameraRotationAxisYaw = Vector3.UnitX;
        public static readonly Vector3 SecurityCameraRotationAxisPitch = Vector3.UnitY;
        public static readonly Vector3 SecurityCameraRotationAxisRoll = Vector3.UnitZ;
        #endregion

        #region Drive Controller
        public static Keys[] PlayerMoveKeys = {Keys.U, Keys.J, Keys.H, Keys.K, Keys.Space, Keys.I,
            Keys.N, Keys.M};
        public static float PlayerMoveSpeed = 0.02f;
        public static float PlayerStrafeSpeed = 0.07f * PlayerMoveSpeed;
        public static float PlayerRotationSpeed = 0.08f;
        #endregion

        #region Menu
        public static readonly Keys KeyPauseShowMenu = Keys.Escape;
        #endregion

        #region Mouse
        //defines how much the mouse has to move in pixels before a movement is registered - see MouseManager::HasMoved()
        public static readonly float MouseSensitivity = 1;

        //always ensure that we start picking OUTSIDE the collidable first person camera radius - otherwise we will always pick ourself!
        public static readonly float PickStartDistance = CollidableCameraCapsuleRadius * 1.1f;
        public static readonly float PickEndDistance = 1000; //can be related to camera far clip plane radius but should be limited to typical level max diameter
        #endregion
    }
}
