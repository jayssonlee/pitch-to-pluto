﻿using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Templates
{
    public class PausableDrawableGameComponent : DrawableGameComponent
    {
        #region Variables
        private StatusType statusType;
        private EventDispatcher eventDispatcher;
        #endregion

        #region Properties 
        public StatusType StatusType
        {
            get
            {
                return this.statusType;
            }
            set
            {
                this.statusType = value;
            }
        }

        public EventDispatcher EventDispatcher
        {
            get
            {
                return this.eventDispatcher;
            }

            set
            {
                this.eventDispatcher = value;
            }
        }
        #endregion
        public PausableDrawableGameComponent(Game game, EventDispatcher eventDispatcher,
            StatusType statusType)
            : base(game)
        {
            //allows us to start the game component with drawing and/or updating paused
            this.eventDispatcher = eventDispatcher;
            this.statusType = statusType;
            RegisterForEventHandling(eventDispatcher);
        }

        #region Event Handling
        protected virtual void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.MenuChanged += EventDispatcher_MenuChanged;
        }

        protected virtual void EventDispatcher_MenuChanged(EventData eventData)
        {

        }
        #endregion

        public override void Update(GameTime gameTime)
        {
            //screen manager needs to listen to input even when paused - see ScreenManager::HandleInput
            HandleInput(gameTime);
            if ((this.statusType & StatusType.Update) != 0) //if update flag is set
            {
                ApplyUpdate(gameTime);
                base.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if ((this.statusType & StatusType.Drawn) != 0) //if draw flag is set
            {
                ApplyDraw(gameTime);
                base.Draw(gameTime);
            }
        }

        protected virtual void ApplyUpdate(GameTime gameTime)
        {

        }

        protected virtual void ApplyDraw(GameTime gameTime)
        {

        }

        protected virtual void HandleInput(GameTime gameTime)
        {

        }
    }
}

