﻿/* Enable CDCR through JigLibX by integrate the forces applied to each collidable object*/
using GDLibrary.Controllers.Physics;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Templates;
using JigLibX.Collision;
using JigLibX.Physics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Physics
{
    public class PhysicsManager : PausableGameComponent
    {
        #region Variables
        private PhysicsSystem physicsSystem;
        private PhysicsController physicsController;
        private float timeStep = 0;
        #endregion

        #region Properties
        public PhysicsSystem PhysicsSystem
        {
            get
            {
                return this.physicsSystem;
            }
        }

        public PhysicsController PhysicsController
        {
            get
            {
                return this.physicsController;
            }
        }
        #endregion

        public PhysicsManager(Game game, EventDispatcher eventDispatcher, StatusType statusType)
            : base(game, eventDispatcher, statusType)
        {
            this.physicsSystem = new PhysicsSystem();

            //add cd/cr system
            this.physicsSystem.CollisionSystem = new CollisionSystemSAP();
            this.physicsSystem.EnableFreezing = true;
            this.physicsSystem.SolverType = PhysicsSystem.Solver.Normal;
            this.physicsSystem.CollisionSystem.UseSweepTests = true;
            //affect accuracy and the overhead 
            //the more accurate the bigger the overhead - more time and CPU resources requirement
            this.physicsSystem.NumCollisionIterations = 8;
            this.physicsSystem.NumContactIterations = 8;
            this.physicsSystem.NumPenetrationRelaxtionTimesteps = 12;

            //affect collision accuracy
            this.physicsSystem.AllowedPenetration = 0.000025f;
            this.physicsSystem.CollisionTollerance = 0.00005f;

            this.physicsController = new PhysicsController();
            this.physicsSystem.AddController(physicsController);
        }

        #region Event Handling
        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start event
            if (eventData.EventCategoryType == EventCategoryType.MainMenu
                && eventData.EventType == EventActionType.OnStart)
            {
                //turn on update
                this.StatusType = StatusType.Update;
            }
            //did the event come from the game and is it a pause game event
            else if (eventData.EventCategoryType == EventCategoryType.MainMenu
                && eventData.EventType == EventActionType.OnPause)
            {
                //turn on update and draw for the menu - the game is paused
                this.StatusType = StatusType.Off;
            }
        }
        #endregion

        protected override void ApplyUpdate(GameTime gameTime)
        {
            timeStep = (float)gameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;
            //If the time between updates indicates an FPS of close to 60fps or less then update CDCR
            if(timeStep < 1.0f/60.0f)
            {
                physicsSystem.Integrate(timeStep);
            }
            else
            {
                //fix at 60 updates a second
                physicsSystem.Integrate(1.0f/60.0f);
            }
        }
    }
}
