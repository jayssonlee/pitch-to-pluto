﻿/* Provides mouse input functions */

using GDLibrary.Actors.Base;
using GDLibrary.Actors.Camera;
using GDLibrary.Managers.Physics;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Input
{
    public class MouseManager : GameComponent
    {
        #region
        private MouseState newState, oldState;
        private PhysicsManager physicsManager;
        #endregion

        //temp local vars
        float frac;
        CollisionSkin skin;

        #region Properties
        public Microsoft.Xna.Framework.Rectangle Bounds
        {
            get
            {
                return new Microsoft.Xna.Framework.Rectangle(this.newState.X, this.newState.Y, 1, 1);
            }
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(this.newState.X, this.newState.Y);
            }
        }

        public bool MouseVisible
        {
            get
            {
                return this.Game.IsMouseVisible;
            }
            set
            {
                this.Game.IsMouseVisible = value;
            }
        }
        #endregion

        public MouseManager(Game game, bool isVisible, PhysicsManager physicsManager)
            : base(game)
        {
            this.Game.IsMouseVisible = isVisible;
            this.physicsManager = physicsManager;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            //store old state
            this.oldState = newState;
            //get new state
            this.newState = Mouse.GetState();
           
            base.Update(gameTime);
        }

        public bool HasMoved(float mouseSensitivity)
        {
            float deltaPositionLength = new Vector2(newState.X -
                oldState.X, newState.Y - oldState.Y).Length();
            return (deltaPositionLength > mouseSensitivity) ? true : false;
        }

        public bool IsLeftButtonClickedOnce()
        {
            return ((newState.LeftButton.Equals(ButtonState.Pressed))
                && (!oldState.LeftButton.Equals(ButtonState.Pressed)));
        }

        public bool IsLeftButtonClicked()
        {
            return (newState.LeftButton.Equals(ButtonState.Pressed));
        }

        public bool IsRightButtonClickedOnce()
        {
            return ((newState.RightButton.Equals(ButtonState.Pressed))
                && (!oldState.RightButton.Equals(ButtonState.Pressed)));
        }

        public bool IsRightButtonClicked()
        {
            return (newState.RightButton.Equals(ButtonState.Pressed));
        }

        //Calculate distance to centre of screen
        public Vector2 GetDeltaFromCentre(Vector2 screenCentre)
        {
            return 3.0f*new Vector2(this.newState.X - screenCentre.X,
                this.newState.Y - screenCentre.Y);
        }

        //has the state changed since the update
        public bool IsStateChanged()
        {
            return (this.newState.Equals(oldState)) ? false : true;
        }

        //did the mouse move beyond its limits of pecision 
        public bool IsStateChangedOutsidePrecision(float mousePrecision)
        {
            return ((Math.Abs(newState.X - oldState.X) > mousePrecision)
                || (Math.Abs(newState.Y - oldState.Y) > mousePrecision));
        }

        //how much has the scroll wheel moved since last update
        public int GetDeltaFromScrollWheel()
        {
            if(IsStateChanged())
            {
                return newState.ScrollWheelValue - oldState.ScrollWheelValue;
            }
            return 0;
        }

        public void SetPosition(Vector2 position)
        {
            Mouse.SetPosition((int)position.X, (int)position.Y);
        }

        //test if the mouse is on the vertical screen edge
        public bool IsMouseOnScreenEdgeVertical(float activationSensitivity, 
            ref Vector2 mouseDelta, Microsoft.Xna.Framework.Rectangle screenRectangle)
        {
            //left
            if(this.newState.X <= (screenRectangle.Width * (1 - activationSensitivity)))
            {
                mouseDelta += Vector2.UnitY;
                return true;
            }
            else if (this.newState.X >= (screenRectangle.Width * activationSensitivity))
            {
                mouseDelta -= Vector2.UnitY;
                return true;
            }
            return false;
        }

        //test if the mouse is on the vertical screen edge
        public bool IsMouseOnScreenEdgeHorizontal(float activationSensitivity,
            ref Vector2 mouseDelta, Microsoft.Xna.Framework.Rectangle screenRectangle)
        {
            //top
            if (this.newState.Y <= (screenRectangle.Height * (1 - activationSensitivity)))
            {
                mouseDelta += Vector2.UnitX;
                return true;
            }
            else if (this.newState.Y >= (screenRectangle.Height * activationSensitivity))
            {
                mouseDelta -= Vector2.UnitX;
                return true;
            }
            return false;
        }

        #region Ray Picking
        //inner class used for ray picking
        class ImmovableSkinPredicate : CollisionSkinPredicate1
        {
            public override bool ConsiderSkin(CollisionSkin skin0)
            {
                if (skin0.Owner != null)
                    return true;
                else
                    return false;
            }
        }
        //get a ray positioned at the mouse's location on the screen - used for picking 
        protected Microsoft.Xna.Framework.Ray GetMouseRay(Camera3D camera)
        {
            //get the positions of the mouse in screen space
            Vector3 near = new Vector3(this.newState.X, this.Position.Y, 0);

            //convert from screen space to world space
            near = camera.Viewport.Unproject(near, camera.ProjectionParameters.Projection, camera.View, Matrix.Identity);

            return GetMouseRayFromNearPosition(camera, near);
        }

        //get a ray from a user-defined near position in world space and the mouse pointer
        protected Microsoft.Xna.Framework.Ray GetMouseRayFromNearPosition(Camera3D camera, Vector3 near)
        {
            //get the positions of the mouse in screen space
            Vector3 far = new Vector3(this.newState.X, this.Position.Y, 1);

            //convert from screen space to world space
            far = camera.Viewport.Unproject(far, camera.ProjectionParameters.Projection, camera.View, Matrix.Identity);

            //generate a ray to use for intersection tests
            return new Microsoft.Xna.Framework.Ray(near, Vector3.Normalize(far - near));
        }

        //get a ray positioned at the screen position - used for picking when we have a centred reticule
        protected Vector3 GetMouseRayDirection(Camera3D camera, Vector2 screenPosition)
        {
            //get the positions of the mouse in screen space
            Vector3 near = new Vector3(screenPosition.X, screenPosition.Y, 0);
            Vector3 far = new Vector3(this.Position, 1);

            //convert from screen space to world space
            near = camera.Viewport.Unproject(near, camera.ProjectionParameters.Projection, camera.View, Matrix.Identity);
            far = camera.Viewport.Unproject(far, camera.ProjectionParameters.Projection, camera.View, Matrix.Identity);

            //generate a ray to use for intersection tests
            return Vector3.Normalize(far - near);
        }

        //used when in 1st person collidable camera mode
        //start distance allows us to start the ray outside the collidable skin of the 1st person colliable camera object
        //otherwise the only thing we would ever collide with would be ourselves!
        public Actor GetPickedObject(Camera3D camera, Vector2 screenPosition, float startDistance, float endDistance, out Vector3 pos, out Vector3 normal)
        {

            Vector3 ray = GetMouseRayDirection(camera, screenPosition);
            ImmovableSkinPredicate pred = new ImmovableSkinPredicate();

            this.physicsManager.PhysicsSystem.CollisionSystem.SegmentIntersect(out frac, out skin, out pos, out normal,
                new Segment(camera.Transform3D.Translation + startDistance * Vector3.Normalize(ray), ray * endDistance), pred);

            if (skin != null && skin.Owner != null)
                return skin.Owner.ExternalData as Actor;

            return null;
        }

        protected Actor GetPickedObject(Camera3D camera, float distance, out Vector3 pos, out Vector3 normal)
        {
            return GetPickedObject(camera, new Vector2(this.newState.X, this.newState.Y), 0, distance, out pos, out normal);
        }

        protected Actor GetPickedObject(Camera3D camera, float startDistance, float distance, out Vector3 pos, out Vector3 normal)
        {
            return GetPickedObject(camera, new Vector2(this.newState.X, this.newState.Y), startDistance, distance, out pos, out normal);
        }

        protected Vector3 GetMouseRayDirection(Camera3D camera)
        {
            return GetMouseRayDirection(camera, new Vector2(this.newState.X, this.newState.Y));
        }
        #endregion
    }
}
