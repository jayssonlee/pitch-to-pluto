﻿using GDLibrary.Actors;
using GDLibrary.Actors.Base;
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Controller.Base;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controllers.Camera3D
{
    public class ThirdPersonController : TargetController
    {
        #region Fields
        private float elevationAngleX, elevationAngleY, distance, scrollSpeedDistanceMultiplier, scrollSpeedElevationMultiplier;

        //used to dampen camera movement
        private Vector3 oldTranslation;
        private Vector3 oldCameraToTarget;
        private float translationLerpSpeed, lookLerpSpeed;
        private MouseManager mouseManager;
        private CameraManager cameraManager;
        private KeyboardManager keyboardManager;
        #endregion

        #region Properties
        public float TranslationLerpSpeed
        {
            get
            {
                return translationLerpSpeed;
            }
            set
            {

                //lerp speed should be in the range >0 and <=1
                translationLerpSpeed = (value > 0) && (value <= 1) ? value : 0.1f;
            }
        }
        public float LookLerpSpeed
        {
            get
            {
                return lookLerpSpeed;
            }
            set
            {

                //lerp speed should be in the range >0 and <=1
                lookLerpSpeed = (value > 0) && (value <= 1) ? value : 0.1f;
            }
        }
        public float ScrollSpeedDistanceMultiplier
        {
            get
            {
                return scrollSpeedDistanceMultiplier;
            }
            set
            {
                //distanceScrollMultiplier should not be lower than 0
                scrollSpeedDistanceMultiplier = (value > 0) ? value : 1;
            }
        }

        public float ScrollSpeedElevationMultiplier
        {
            get
            {
                return scrollSpeedElevationMultiplier;
            }
            set
            {
                //scrollSpeedElevationMulitplier should not be lower than 0
                scrollSpeedElevationMultiplier = (value > 0) ? value : 1;
            }
        }

        public float Distance
        {
            get
            {
                return distance;
            }
            set
            {
                //distance should not be lower than 0
                distance = (value > 0) ? value : 1;
            }
        }
        public float ElevationAngleX
        {
            get
            {
                return elevationAngleX;
            }
            set
            {
                elevationAngleX = value % 360;
                elevationAngleX = MathHelper.ToRadians(elevationAngleX);
            }
        }

        public float ElevationAngleY
        {
            get
            {
                return elevationAngleY;
            }
            set
            {
                elevationAngleY = value % 360;
                elevationAngleY = MathHelper.ToRadians(elevationAngleY);
            }
        }
        #endregion

        public ThirdPersonController(string id, ControllerType controllerType, Actor targetActor,
            float distance, float scrollSpeedDistanceMultiplier, float elevationAngle,
                float scrollSpeedElevationMultiplier, float translationLerpSpeed, float lookLerpSpeed, KeyboardManager keyboardManager, MouseManager mouseManager, CameraManager cameraManager)
            : base(id, controllerType, targetActor)
        {
            //call properties to set validation on distance and radian conversion
            this.Distance = distance;



            //allows us to control distance and elevation from the mouse scroll wheel
            this.ScrollSpeedDistanceMultiplier = scrollSpeedDistanceMultiplier;
            this.ScrollSpeedElevationMultiplier = scrollSpeedElevationMultiplier;
            //notice that we pass the incoming angle through the property to convert it to radians
            //this.ElevationAngle = elevationAngle;

            //dampen camera translation reaction
            this.translationLerpSpeed = translationLerpSpeed;
            //dampen camera rotation reaction
            this.lookLerpSpeed = lookLerpSpeed;

            this.keyboardManager = keyboardManager;

            //used to change elevation angle or distance from target - See UpdateFromScrollWheel()
            this.mouseManager = mouseManager;

            this.cameraManager = cameraManager;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            UpdateMouse(gameTime);
            UpdateFromScrollWheel(gameTime);
            //UpdateFromKeyboard(gameTime);
            UpdateParent(gameTime, actor as Actor3D, this.TargetActor as Actor3D);
        }

        private void UpdateParent(GameTime gameTime, Actor3D parentActor, Actor3D targetActor)
        {
            if (targetActor != null)
            {
                Vector3 cameraToTarget, cameraToTargetY;
                //rotate the target look around the target right to get a vector pointing away from the target at a specified elevation
                cameraToTarget = Vector3.Transform(targetActor.Transform3D.Look,
                    Matrix.CreateFromAxisAngle(parentActor.Transform3D.Up, this.ElevationAngleX));

                cameraToTargetY = Vector3.Transform(targetActor.Transform3D.Look,
                    Matrix.CreateFromAxisAngle(parentActor.Transform3D.Right, this.ElevationAngleY));

                //normalize to give unit length, otherwise distance from camera to target will vary over time
                cameraToTarget += cameraToTargetY;
                cameraToTarget.Normalize();
                cameraToTargetY.Normalize();

                if (targetActor is CollidableObject)
                    targetActor.Transform3D.Translation = ((CollidableObject)targetActor).Body.Position;

                //set the position of the camera to be a set distance from target and at certain elevation angle
                parentActor.Transform3D.Translation = Vector3.Lerp(this.oldTranslation, (cameraToTarget) * this.distance + targetActor.Transform3D.Translation, this.translationLerpSpeed);

                //set the camera to look at the target object
                parentActor.Transform3D.Look = Vector3.Lerp(this.oldCameraToTarget, -cameraToTarget, this.lookLerpSpeed);

                //store old values for lerp
                this.oldTranslation = parentActor.Transform3D.Translation;
                this.oldCameraToTarget = -cameraToTarget;
            }
        }

        private void UpdateFromScrollWheel(GameTime gameTime)
        {
            //get magnitude and direction of scroll change
            float scrollWheelDelta = -this.mouseManager.GetDeltaFromScrollWheel() * gameTime.ElapsedGameTime.Milliseconds;
            //move camera closer to, or further, from the target
            this.Distance += this.scrollSpeedDistanceMultiplier * scrollWheelDelta;

            if (this.Distance <= 10)
            {
                this.Distance = 10;
            }
            else if (this.Distance >= 200)
            {
                this.Distance = 200;
            }

            //try uncommenting this line and see how we can affect the elevation angle also
            //this.ElevationAngle += this.scrollSpeedElevationMultiplier * scrollWheelDelta;
        }
        private void UpdateMouse(GameTime gameTime)
        {
            Vector2 mouseDelta;
            mouseDelta = this.mouseManager.GetDeltaFromCentre(new Vector2(1280 / 2, 720 / 2));

            float xDist = mouseDelta.X / 1920 * 160;
            float yDist = mouseDelta.Y / 1080 * 80;
            //System.Diagnostics.Debug.WriteLine(xDist +" "+ yDist +" "+ mouseDelta.X + " " + mouseDelta.Y);
            this.ElevationAngleX = xDist;
            this.ElevationAngleY = yDist;
            //System.Diagnostics.Debug.WriteLine(this.ElevationAngleX + " "+ this.ElevationAngleY + " "+ xDist + " " + yDist);
        }

        private void UpdateFromKeyboard(GameTime gameTime)
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.A))
            {
                //ElevationAngleX += 5;
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.D))
            {
                //ElevationAngleX -= 5;
            }
        }

        public override bool Equals(object obj)
        {
            ThirdPersonController other = obj as ThirdPersonController;

            if (other == null)
                return false;
            else if (this == other)
                return true;

            return this.elevationAngleX.Equals(other.ElevationAngleX)
                    && this.distance.Equals(other.ElevationAngleY)
                    && this.distance.Equals(other.Distance)
                      && this.translationLerpSpeed.Equals(other.TranslationLerpSpeed)
                        && this.lookLerpSpeed.Equals(other.LookLerpSpeed)
                        && base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.elevationAngleX.GetHashCode();
            hash = hash * 37 + this.elevationAngleY.GetHashCode();
            hash = hash * 17 + this.distance.GetHashCode();
            hash = hash * 41 + this.translationLerpSpeed.GetHashCode();
            hash = hash * 47 + this.lookLerpSpeed.GetHashCode();
            hash = hash * 11 + base.GetHashCode();
            return hash;
        }

        //be careful when cloning this controller as we will need to reset the target actor - assuming the clone attaches to a different target
        public override object Clone()
        {
            return new ThirdPersonController("clone - " + this.ID, //deep
                this.ControllerType, //deep
                this.TargetActor as Actor, //shallow - a ref
                this.distance, //deep
                this.scrollSpeedDistanceMultiplier, //deep
                this.elevationAngleX, //deep
                this.scrollSpeedElevationMultiplier, //deep
                this.translationLerpSpeed, //deep
                this.lookLerpSpeed, //deep
                this.keyboardManager,
                this.mouseManager,
                this.cameraManager); //shallow - a ref
        }
    }
}
