﻿/* Security cameras pan cyclicly with a panning speed*/

using GDLibrary.Controllers.Base;
using GDLibrary.Enums;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Interfaces;
using GDLibrary.Actors;

namespace GDLibrary.Controllers.Camera3D
{
    public class SecurityCameraController : GDLibrary.Controller.Base.Controller
    {
        #region Variables
        private float rotationAmplitude;
        private float rotationSpeedMultiplier;
        private Vector3 rotationAxis;
        #endregion

        #region Properties
        public float RotationAmplitude
        {
            get
            {
                return this.rotationAmplitude;
            }

            set
            {
                //clamp to prevent <=0 values
                this.rotationAmplitude = (value > 0) ? value : 1;
            }
        }

        public float RotationSpeedMultiplier
        {
            get
            {
                return this.rotationSpeedMultiplier;
            }

            set
            {
                //clamp to prevent <=0 values
                this.rotationSpeedMultiplier = (value > 0) ? value : 1;
            }
        }

        public Vector3 RotationAxis
        {
            get
            {
                return this.rotationAxis;
            }

            set
            {
                //clamp to prevent vector length = 0
                this.rotationAxis = (value != Vector3.Zero) ? value : Vector3.UnitX;
                //Always want this to be normalized
                this.rotationAxis.Normalize();
            }
        }
        #endregion

        public SecurityCameraController(string id, ControllerType controllerType,
            float rotationAmplitude, float rotationSpeedMultiplier, Vector3 rotationAxis)
            :base(id, controllerType)
        {
            this.RotationAmplitude = rotationAmplitude;
            this.RotationSpeedMultiplier = rotationSpeedMultiplier;
            this.RotationAxis = rotationAxis;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            //limit the angle to 360 using a modulus
            float time = (float)gameTime.TotalGameTime.TotalSeconds % 360;
            //bounded the angle about which to rotate the camera
            float boundedRotationAngle = this.rotationAmplitude *
                (float)Math.Sin(this.rotationSpeedMultiplier * time);
            //Debug
            System.Diagnostics.Debug.WriteLine("boundedRotationAngle: " + boundedRotationAngle);
            //cast the actor to the Actor3D
            Actor3D actor3D = actor as Actor3D;
            //Apply the rotation to the camera
            actor3D.Transform3D.RotateBy(this.rotationAxis * boundedRotationAngle);
            base.Update(gameTime, actor);
        }



    }
}
