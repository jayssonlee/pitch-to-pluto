﻿/* Flight camera controller, allow movement in XYZ plane*/
//HOMEWORK Test this out
//To test go to main look at the first person camera works
//Either add a new camera or just change existing controller
//from a first person camera to a FlightCameraController
//Note: You will need to add controls for flight

using GDLibrary.Controllers.Base;
using GDLibrary.Enums;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Input;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Actors;
using Microsoft.Xna.Framework;

namespace GDLibrary.Controllers.Camera3D
{
    public class FlightCameraController : UserInputController
    {
        #region Variables
        private CameraManager cameraManager;
        #endregion

        #region Properties
        #endregion

        public FlightCameraController(string id, ControllerType controllerType, Keys[] moveKeys,
            float moveSpeed, float strafeSpeed, float rotationSpeed, MouseManager mouseManager,
            KeyboardManager keyboardManager, CameraManager cameraManager)
            : base(id, controllerType, moveKeys, moveSpeed, strafeSpeed, rotationSpeed, mouseManager,
                  keyboardManager)
        {
            /* Used to access the viewport for the active camera. Knowing the centre of the 
             * active viewport allows us to determine how far far from the centre the mouse
             * We need to determine this to know how much we should rotate in HandleMouseInput*/
            this.cameraManager = cameraManager;
        }

        public override void HandleMouseInput(GameTime gameTime, Actor3D parentActor)
        {
            Vector2 mouseDelta = Vector2.Zero;

            mouseDelta = -this.MouseManager.GetDeltaFromCentre(this.cameraManager.ActiveCamera.ViewportCentre);
            mouseDelta *= gameTime.ElapsedGameTime.Milliseconds;
            mouseDelta *= this.RotationSpeed;

            //only rotate if something has chaned with the mouse
            if (mouseDelta.Length() != 0)
            {
                //Console.WriteLine(mouseDelta.X.ToString(), mouseDelta.Y.ToString());
                parentActor.Transform3D.RotateBy(new Vector3(mouseDelta.X, mouseDelta.Y, 0));
            }
        }

        public override void HandleKeyboardInput(GameTime gameTime, Actor3D parentActor)
        {
            Vector3 translation = Vector3.Zero;
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[0]))
            {
                translation = gameTime.ElapsedGameTime.Milliseconds * this.MoveSpeed *
                    parentActor.Transform3D.Look;
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[1]))
            {
                translation = -gameTime.ElapsedGameTime.Milliseconds * this.MoveSpeed *
                    parentActor.Transform3D.Look;
            }

            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[2]))
            {
                translation += -gameTime.ElapsedGameTime.Milliseconds * this.StrafeSpeed *
                    parentActor.Transform3D.Right;
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[3]))
            {
                translation += gameTime.ElapsedGameTime.Milliseconds * this.StrafeSpeed *
                    parentActor.Transform3D.Right;
            }
            parentActor.Transform3D.TranslateBy(translation);
            //Was a move button pressed
            //if (translation != Vector3.Zero)
            //{
            //    //remove the y-axis component of the translation
            //    translation.Y = 0;
            //    parentActor.Transform3D.TranslateBy(translation);
            //}

        }
    }
}

