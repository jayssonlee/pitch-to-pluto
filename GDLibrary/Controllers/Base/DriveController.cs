﻿/* Creates a simple controller for drivable objects taking input from keyboard */
using GDLibrary.Controllers.Base;
using GDLibrary.Enums;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Input;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Actors;
using Microsoft.Xna.Framework;
using GDLibrary.Interfaces;

namespace GDLibrary.Controllers.Camera3D
{
    public class DriveController : UserInputController
    {
        #region Variables
        #endregion

        #region Properties
        #endregion

        public DriveController(string id, ControllerType controllerType, Keys[] moveKeys,
            float moveSpeed, float strafeSpeed, float rotationSpeed, MouseManager mouseManager,
            KeyboardManager keyboardManager)
            : base(id, controllerType, moveKeys, moveSpeed, strafeSpeed, rotationSpeed, mouseManager,
                  keyboardManager)
        {
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            base.Update(gameTime, actor);
        }

        public override void HandleKeyboardInput(GameTime gameTime, Actor3D parentActor)
        {
            Vector3 translation = Vector3.Zero;
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[0]))
            {
                translation = gameTime.ElapsedGameTime.Milliseconds * this.MoveSpeed *
                    parentActor.Transform3D.Look;
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[1]))
            {
                translation = -gameTime.ElapsedGameTime.Milliseconds * this.MoveSpeed *
                    parentActor.Transform3D.Look;
            }

            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[4]))
            {
                translation += gameTime.ElapsedGameTime.Milliseconds * this.StrafeSpeed *
                    parentActor.Transform3D.Right;
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[5]))
            {
                translation += -gameTime.ElapsedGameTime.Milliseconds * this.StrafeSpeed *
                    parentActor.Transform3D.Right;
            }
            //rotate
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[2]))
            {
                parentActor.Transform3D.RotateAroundYBy(gameTime.ElapsedGameTime.Milliseconds * this.RotationSpeed);
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[3]))
            {
                parentActor.Transform3D.RotateAroundYBy(-gameTime.ElapsedGameTime.Milliseconds * this.RotationSpeed);
            }

            //Was a move button pressed
            if (translation != Vector3.Zero)
            {
                //remove the y-axis component of the translation
                translation.Y = 0;
                parentActor.Transform3D.TranslateBy(translation);
            }

        }
    }
}

