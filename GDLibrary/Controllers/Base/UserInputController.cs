﻿/* Parent class for all controllers that accept keyboard input and apply to an actor
 * e.g FirstPersonCameraController inherits from this class */
using GDLibrary.Enums;
using GDLibrary.Managers.Input;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Interfaces;
using Microsoft.Xna.Framework;
using GDLibrary.Actors;

namespace GDLibrary.Controllers.Base
{
    public class UserInputController : GDLibrary.Controller.Base.Controller
    {
        #region Variables
        private Keys[] moveKeys;
        private float moveSpeed, strafeSpeed, rotationSpeed;
        private MouseManager mouseManager;
        private KeyboardManager keyboardManager;
        #endregion

        #region Properties
        public Keys[] MoveKeys
        {
            get
            {
                return this.moveKeys;
            }

            set
            {
                this.moveKeys = value;
            }
        }

        public float MoveSpeed
        {
            get
            {
                return this.moveSpeed;
            }

            set
            {
                this.moveSpeed = value;
            }
        }

        public float StrafeSpeed
        {
            get
            {
                return this.strafeSpeed;
            }

            set
            {
                this.strafeSpeed = value;
            }
        }

        public float RotationSpeed
        {
            get
            {
                return this.rotationSpeed;
            }

            set
            {
                this.rotationSpeed = value;
            }
        }

        public MouseManager MouseManager
        {
            get
            {
                return this.mouseManager;
            }
        }

        public KeyboardManager KeyboardManager
        {
            get
            {
                return this.keyboardManager;
            }
        }
        #endregion

        public UserInputController(string id, ControllerType controllerType, Keys[] moveKeys,
            float moveSpeed, float strafeSpeed, float rotationSpeed, MouseManager mouseManager,
            KeyboardManager keyboardManager)
            :base(id, controllerType)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.strafeSpeed = strafeSpeed;
            this.rotationSpeed = rotationSpeed;

            this.mouseManager = mouseManager;
            this.keyboardManager = keyboardManager;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor3D parentActor = actor as Actor3D;
            HandleMouseInput(gameTime, parentActor);
            HandleKeyboardInput(gameTime, parentActor);
            base.Update(gameTime, actor);
        }

        public virtual void HandleMouseInput(GameTime gameTime, Actor3D parentActor)
        {
            //Do nothing implement in child class
        }

        public virtual void HandleKeyboardInput(GameTime gameTime, Actor3D parentActor)
        {
            //Do nothing implement in child class
        }

    }
}
