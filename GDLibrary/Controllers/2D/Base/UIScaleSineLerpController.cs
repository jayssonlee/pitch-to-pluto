﻿using GDLibrary.Actors.Drawn._2D.UI;
using GDLibrary.Actors.Drawn2D.UI;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Parameters.Other;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controller._2D.Base
{
    public class UIScaleSineLerpController : UIController
    {
        #region Fields
        private TrigonometricParameters trigonometricParameters;
        #endregion

        #region Properties
        public TrigonometricParameters TrigonometricParameters
        {
            get
            {
                return this.trigonometricParameters;
            }
            set
            {
                this.trigonometricParameters = value;
            }
        }
        #endregion

        public UIScaleSineLerpController(string id, ControllerType controllerType, TrigonometricParameters trigonometricParameters)
            : base(id, controllerType)
        {
            this.TrigonometricParameters = trigonometricParameters;
        }

        public override void SetActor(IActor actor)
        {
            UIObject uiObject = actor as UIObject;
            uiObject.Transform.Scale = uiObject.Transform.OriginalTransform2D.Scale;
        }

        protected override void ApplyController(GameTime gameTime, UIObject uiObject, float totalElapsedTime)
        {
            //sine wave in the range 0 -> max amplitude
            float lerpFactor = MathUtility.SineLerpByElapsedTime(this.TrigonometricParameters, totalElapsedTime);
            //apply scale change
            uiObject.Transform.Scale = uiObject.Transform.OriginalTransform2D.Scale + Vector2.One * lerpFactor;
        }

        public override bool Equals(object obj)
        {
            UIScaleSineLerpController other = obj as UIScaleSineLerpController;

            if (other == null)
                return false;
            else if (this == other)
                return true;

            return this.trigonometricParameters.Equals(other.TrigonometricParameters)
                    && base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.ID.GetHashCode();
            hash = hash * 17 + this.trigonometricParameters.GetHashCode();
            return hash;
        }

        public override object Clone()
        {
            return new UIScaleSineLerpController("clone - " + this.ID, //deep
                this.ControllerType, //deep
                (TrigonometricParameters)this.trigonometricParameters.Clone() //deep
                );
        }
    }
}

