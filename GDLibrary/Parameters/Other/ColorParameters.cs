﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Parameters.Other
{
    public class ColorParameters : ICloneable
    {
        #region Statics
        public static ColorParameters WhiteOpaque = new ColorParameters(Color.White, 1);
        public static ColorParameters WhiteAlmostOpaque = new ColorParameters(Color.White, 0.99f);
        #endregion

        #region Fields
        private Color color;
        private float alpha;
        private ColorParameters originalColorParameters;
        #endregion

        #region Properties
        public Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }
        public float Alpha
        {
            get
            {
                return this.alpha;
            }
            set
            {
                if(value < 0)
                {
                    this.alpha = 0;
                }
                else if(value > 1)
                {
                    this.alpha = 1;
                }
                else
                {
                    this.alpha = (float)Math.Round(value, 4);
                }
            }
        }
        public ColorParameters OriginalColorParameters
        {
            get
            {
                return this.originalColorParameters;
            }
        }
        #endregion

        public ColorParameters(Color color, float alpha)
        {
            Initialize(color, alpha);

            //store original values in case of reset
            this.originalColorParameters = new ColorParameters();
            this.originalColorParameters.Initialize(color, alpha);
        }

        public ColorParameters()
        {

        }

        protected void Initialize(Color color, float alpha)
        {
            this.color = color;
            this.Alpha = alpha;
        }

        public void Reset()
        {
            Initialize(this.originalColorParameters.Color, this.originalColorParameters.Alpha);
        }

        public override bool Equals(object obj)
        {
            ColorParameters other = obj as ColorParameters;
            return this.color == other.Color && this.alpha == other.Alpha;
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.color.GetHashCode();
            hash = (int)(hash * 17 + this.alpha);
            return hash;
        }

        public object Clone()
        {
            //deep because all variables are either C# types (e.g. primitives, structs, or enums) or  XNA types
            return this.MemberwiseClone();
        }
    }
}

