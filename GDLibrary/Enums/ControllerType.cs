﻿/* Used by controller to defines what action the controller applies to its actor
 * For example a FirstPersonCamera has an atttached controller of type FirstPerson*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum ControllerType : sbyte
    {
        Drive, //applied to the model
        FirstPerson, //applied to the camera
        ThirdPerson, //applied to the camera
        Rail, //applied to camera or model
        Track, //applied to camera or model   
        Security, //applied to camera
        CollidableFirstPerson,
        
        //applied to any actor (camera or model)
        Rotation,
        Translation, 
        Scale,
        LerpRotation,
        LerpTranslation,
        LerpScale,
        LerpColor,
        LerpTexture,

        SineScaleLerp,
        SineRotationLerp,
        SineColorLerp
    }
}
