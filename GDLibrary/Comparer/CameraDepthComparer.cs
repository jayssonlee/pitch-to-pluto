﻿/* Used by the CameraManager::SortByDepth to sort the camera
 * by depth to allow PIP effects*/
using GDLibrary.Actors.Camera;
using GDLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Comparer
{
    public class CameraDepthComparer : IComparer<Camera3D>
    {
        private SortDirectionType sortDirectionType;

        public CameraDepthComparer(SortDirectionType sortDirectionType)
        {
            this.sortDirectionType = sortDirectionType;
        }

        public int Compare(Camera3D first, Camera3D second)
        {
            float diff = first.DrawDepth - second.DrawDepth;

            if(this.sortDirectionType == SortDirectionType.Descending)
            {
                diff *= -1;
            }

            if(diff < 0)
            {
                return -1;
            }
            else if(diff > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
