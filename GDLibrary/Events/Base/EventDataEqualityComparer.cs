﻿using GDLibrary.Actors.Base;
using GDLibrary.Events.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Events.Base
{
    //used by the EventDispatcher to compare events in a HashSet
    //The HashSet stops events being added to the set twice
    public class EventDataEqualityComparer : IEqualityComparer<EventData>
    {
        public bool Equals(EventData e1, EventData e2)
        {
            return e1.ID.Equals(e2.ID)
                && e1.EventType.Equals(e2.EventType)
                && e1.EventCategoryType.Equals(e2.EventCategoryType)
                && (e1.Sender as Actor).GetID().Equals(e2.Sender as Actor);
        }

        public int GetHashCode(EventData e)
        {
            return e.GetHashCode();
        }
    }
}
