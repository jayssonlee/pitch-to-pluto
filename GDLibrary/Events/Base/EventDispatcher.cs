﻿using GDLibrary.Enums;
using GDLibrary.Events.Data;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Events.Base
{
    public class EventDispatcher : GameComponent
    {
        private static Stack<EventData> stack; //store events in arrival sequence
        private static HashSet<EventData> uniqueSet; //preventt he same event from existing on the stack for a single update cycle

        //a delegate is basically a list of function pointers
        //the function pointer comes from the object that wants
        //to be notified when the event happens
        public delegate void CameraEventHandler(EventData eventData);
        public delegate void MenuEventHandler(EventData eventData);
        public delegate void DebugEventHandler(EventData eventData);
        public delegate void OpacityEventHandler(EventData eventData);
        public delegate void RemoveActorEventHandler(EventData eventData);
        public delegate void GlobalSoundEventHandler(EventData eventData);
        public delegate void Sound3DEventHandler(EventData eventData);
        public delegate void Sound2DEventHandler(EventData eventData);

        /* An event is either null (not happened yet) or non-null
         * When the event occures the delegate will read through its list
         * and call all of the listening functions*/

        public event CameraEventHandler CameraChanged;
        public event MenuEventHandler MenuChanged;
        public event DebugEventHandler DebugChanged;
        public event OpacityEventHandler OpacityChanged;
        public event RemoveActorEventHandler RemoveActorChanged;
        public event Sound3DEventHandler Sound3DChanged;
        public event Sound2DEventHandler Sound2DChanged;
        public event GlobalSoundEventHandler GlobalSoundChanged;

        public EventDispatcher(Game game, int initialSize)
            : base(game)
        {
            stack = new Stack<EventData>(initialSize);
            uniqueSet = new HashSet<EventData>(new EventDataEqualityComparer());
        }

        public static void Publish(EventData eventData)
        {
            //prevent the same event from being published multiple times
            //in a single update e.g 10x bell ring sounds
            if (!uniqueSet.Contains(eventData))
            {
                stack.Push(eventData);
                uniqueSet.Add(eventData);
            }
        }

        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < stack.Count; i++)
            {
                Process(stack.Pop());
            }
            //stack.Clear(); ? Do we need this? Pop should remove all elements
            uniqueSet.Clear();
            base.Update(gameTime);
        }

        private void Process(EventData eventData)
        {
            //One case for each category type
            switch (eventData.EventCategoryType)
            {
                case Enums.EventCategoryType.Camera:
                    OnCamera(eventData);
                    break;

                case Enums.EventCategoryType.MainMenu:
                    OnMenu(eventData);
                    break;

                case EventCategoryType.Debug:
                    OnDebug(eventData);
                    break;

                case EventCategoryType.Opacity:
                    OnOpacity(eventData);
                    break;

                case EventCategoryType.SystemRemove:
                    OnRemoveActor(eventData);
                    break;

                default:
                    break;
            }
        }

        //called when a menu change is requested
        protected virtual void OnMenu(EventData eventData)
        {
            //non-null if an object subscribed for this event
            if (MenuChanged != null)
            {
                MenuChanged(eventData);
            }
        }

        //called when a menu change is requested
        protected virtual void OnCamera(EventData eventData)
        {
            //non-null if an object subscribed for this event
            if (CameraChanged != null)
            {
                CameraChanged(eventData);
            }
        }

        //called when a debug related event occurs (e.g. show/hide debug info)
        protected virtual void OnDebug(EventData eventData)
        {
            DebugChanged?.Invoke(eventData);
        }

        //called when a drawm objects opacity changes
        protected virtual void OnOpacity(EventData eventData)
        {
            OpacityChanged?.Invoke(eventData);
        }

        //called when a drawn objects needs to be removed - see UIMouseObject::HandlePickedObject()
        protected virtual void OnRemoveActor(EventData eventData)
        {
            RemoveActorChanged?.Invoke(eventData);
        }

        //called when a global sound event is sent to set volume by category or mute all sounds
        protected virtual void OnGlobalSound(EventData eventData)
        {
            GlobalSoundChanged?.Invoke(eventData);
        }

        //called when a 3D sound event is sent e.g. play "boom"
        protected virtual void OnSound3D(EventData eventData)
        {
            Sound3DChanged?.Invoke(eventData);
        }

        //called when a 2D sound event is sent e.g. play "menu music"
        protected virtual void OnSound2D(EventData eventData)
        {
            Sound2DChanged?.Invoke(eventData);
        }
    }
}
