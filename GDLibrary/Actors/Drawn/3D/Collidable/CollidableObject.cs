﻿using GDLibrary.Enums;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using JigLibX.Collision;
using JigLibX.Physics;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using JigLibX.Geometry;
using JigLibX.Math;

namespace GDLibrary.Actors.Drawn._3D.Collidable
{
    public class CollidableObject : ModelObject
    {
        #region Variables
        private Body body;
        private CollisionSkin collision;
        private float mass;
        #endregion

        #region Properties
        public Body Body
        {
            get
            {
                return this.body;
            }

            set
            {
                this.body = value;
            }
        }

        public CollisionSkin Collision
        {
            get
            {
                return this.collision;
            }

            set
            {
                this.collision = value;
            }
        }

        public float Mass
        {
            get
            {
                return this.mass;
            }

            set
            {
                this.mass = value;
            }
        }

        #endregion
        //Need to update ModelObject to use ColorParameters
        public CollidableObject(string id, ActorType actorType, Transform3D transform, Effect effect, ColorParameters colorParameters,
            Texture2D texture, Model model)
            :base(id, actorType, transform, effect, colorParameters, texture, model)
        {
            this.body = new Body();
            this.body.ExternalData = this;
            this.collision = new CollisionSkin(this.body);
            this.body.CollisionSkin = this.collision;

            //register for callback collision
            //we will normally register for this event in a sublcass of CollidableObject
            //e.g PickupCollidableObject or PlayerCollidableObject
            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
        }

        public override Matrix GetWorldMatrix()
        {
            return Matrix.CreateScale(this.Transform3D.Scale) *
                this.collision.GetPrimitiveLocal(0).Transform.Orientation *
                this.body.Orientation * this.Transform3D.Orientation *
                Matrix.CreateTranslation(this.body.Position);
        }

        //we will normally provide the callback function in a class that inherits from this class
        public virtual bool CollisionSkin_callbackFn(CollisionSkin skin0, CollisionSkin skin1)
        {
            return true;
        }

        protected Vector3 SetMass(float mass)
        {
            PrimitiveProperties primitiveProperties = new PrimitiveProperties(PrimitiveProperties.MassDistributionEnum.Solid,
                PrimitiveProperties.MassTypeEnum.Density, mass);

            float massLocal;
            Vector3 com;
            Matrix it, itCoM;

            this.collision.GetMassProperties(primitiveProperties, out massLocal, out com, out it, out itCoM);
            body.BodyInertia = itCoM;
            body.Mass = massLocal;
            return com;
        }
        
        public void AddPrimitive(Primitive primitive, MaterialProperties materialProperties)
        {
            this.collision.AddPrimitive(primitive, materialProperties);
        }

        public virtual void Enable(bool bImmovable, float mass)
        {
            this.mass = mass;
            //set whether the object can move
            this.body.Immovable = bImmovable;
            //calculate the centre of mass
            Vector3 com = SetMass(mass);
            //adjust the skin so that it corresponds to the 3D mesh as drawn on screen
            this.body.MoveTo(this.Transform3D.Translation, Matrix.Identity);
            //set the centre of mass
            this.collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));
            //enable so that any applied forces e.g gravity will affect the object
            this.body.EnableBody();
        }

        public new object Clone()
        {
            return new CollidableObject("clone - " + ID, //deep
                this.ActorType,   //deep
                (Transform3D)this.Transform3D.Clone(),  //deep
                this.Effect, //shallow i.e. a reference
                (ColorParameters)this.ColorParameters.Clone(),  //deep
                this.Texture, //shallow i.e. a reference
                this.Model); //shallow i.e. a reference
        }

        public override bool Remove()
        {
            //what would happen if we did not remove the physics body? would the CD/CR skin remain?
            //game.PhysicsManager.PhysicsSystem.RemoveBody(this.body);
            this.body = null;

            return base.Remove();
        }
    }
}
