﻿/* Parent class for all updateable and drawn 3D game object. 
 * Notice that Effect has been added*/

using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._3D
{
    public class DrawnActor3D : Actor3D, ICloneable
    {
        #region Variables
        private Effect effect;
        private ColorParameters colorParameters;
        #endregion

        #region Properties

        public ColorParameters ColorParameters
        {
            get
            {
                return this.colorParameters;
            }
            set
            {
                this.colorParameters = value;
            }
        }
        public Effect Effect
        {
            get
            {
                return this.effect;
            }

            set
            {
                this.effect = value;
            }
        }

        public float Alpha
        {
            get
            {
                return this.colorParameters.Alpha;
            }
            set
            {
                //opaque to transparent
                if(this.colorParameters.Alpha == 1 && value < 1)
                {
                    EventDispatcher.Publish(new EventData("OpTr", this, EventActionType.OnOpaqueToTransparent, 
                        EventCategoryType.Opacity));
                }
                //transparent to opaque
                else if (this.colorParameters.Alpha < 1 && value == 1)
                {
                    EventDispatcher.Publish(new EventData("TrOp", this, EventActionType.OnTransparentToOpaque,
                        EventCategoryType.Opacity));
                }
                this.colorParameters.Alpha = value;

            }
        }

        #endregion

        //used when I don't want to specify a color and an alpha
        public DrawnActor3D(string id, ActorType actorType, 
            Transform3D transform, Effect effect) 
            : this(id, actorType, transform, effect, ColorParameters.WhiteOpaque, StatusType.Drawn | StatusType.Update) //drawn and updated
        {
        }

        public DrawnActor3D(string id, ActorType actorType,
            Transform3D transform, Effect effect, ColorParameters colorParameters, StatusType statusType)
            : base(id, actorType, transform, statusType) //drawn and updated
        {
            this.effect = effect;
            this.colorParameters = colorParameters;
        }

        public override float GetAlpha()
        {
            return this.colorParameters.Alpha;
        }

        public new object Clone()
        {
            return new DrawnActor3D("Clone - " + ID,
                this.ActorType,
                (Transform3D)this.Transform3D.Clone(),
                this.effect,
                (ColorParameters)this.ColorParameters.Clone(),
                StatusType.Drawn | StatusType.Update);
        }

    }
}
