﻿/* Allows us to draw model objects. These are the fbx files
 * that you will import from 3dsmax */
using GDLibrary.Enums;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._3D
{
    public class ModelObject : DrawnActor3D, ICloneable
    {
        #region Variables
        private Texture2D texture;
        private Model model;
        private Matrix[] boneTransforms;

        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }

            set
            {
                this.texture = value;
            }
        }

        public Model Model
        {
            get
            {
                return this.model;
            }

            set
            {
                this.model = value;
            }
        }

        public Matrix[] BoneTransforms
        {
            get
            {
                return this.boneTransforms;
            }

            set
            {
                this.boneTransforms = value;
            }
        }

        public BoundingSphere BoundingSphere
        {
            get
            {
                return this.model.Meshes[model.Root.Index].BoundingSphere.Transform(this.GetWorldMatrix());
            }
        }
        #endregion

        public ModelObject(string id, ActorType actorType, 
            Transform3D transform, Effect effect, ColorParameters colorParameters, Texture2D texture, Model model)
            :base(id, actorType, transform, effect, colorParameters, StatusType.Drawn | StatusType.Update)
        {
            this.texture = texture;
            this.model = model;

            /* 3DS Max models contain meshes (a table might have 
             * 5 meshes - a top and four legs) and each mesh contains a
             * bone. A bone holds the transform that says "move the 
             * mesh to a particular position". Without 5 bones in the 
             * table the table would be a mess*/
            InitializeBoneTransforms();
        }

        private void InitializeBoneTransforms()
        {
            //load bone transforms from the model and copy
            //to our array
            if(this.model != null)
            {
                this.boneTransforms = new Matrix[this.model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(this.boneTransforms);
            }
        }

        public new object Clone()
        {
            return new ModelObject("Clone - " + ID,
                this.ActorType,
                (Transform3D)this.Transform3D.Clone(),
                this.Effect,
                (ColorParameters)this.ColorParameters.Clone(),
                this.texture,
                this.model);
        }

        public override bool Remove()
        {
            //tag for garbage collection
            this.boneTransforms = null;
            return base.Remove();
        }


    }
}
