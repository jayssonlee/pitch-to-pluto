﻿using GDLibrary.Actors.Camera;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Object;
using GDLibrary.Managers.Screen;
using GDLibrary.Templates;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._2D.HUD
{
    public class HUD : PausableDrawableGameComponent
    {
        private CameraManager cameraManager;
        private SpriteFont spriteFont;
        private ScreenManager screenManager;
        private ObjectManager objectManager;
        private SpriteBatch spriteBatch;
        private Color textColor;
        private int strokeCounter;
        private float angleValue;
        private float powerValue;
        private bool win;
        private StringBuilder strokeText;
        private Vector2 hudPosition;
        private Vector2 textPosition;
        private float textWidth;

        public HUD(Game game, ScreenManager screenManager, CameraManager cameraManager, ObjectManager objectManager,
            SpriteBatch spriteBatch, SpriteFont spriteFont, Color textColor, Vector2 hudPosition,
            EventDispatcher eventDispatcher, StatusType statusType, int strokeCounter, float angleValue, float powerValue)
            : base(game, eventDispatcher, statusType)
        {
            this.screenManager = screenManager;
            this.cameraManager = cameraManager;
            this.objectManager = objectManager;
            this.spriteBatch = spriteBatch;
            this.spriteFont = spriteFont;
            this.textColor = textColor;
            this.strokeCounter = strokeCounter;
            this.angleValue = angleValue;
            this.powerValue = powerValue;
            this.hudPosition = hudPosition;
            this.strokeText = new StringBuilder("Strokes: " + strokeCounter + " ");
            this.textWidth = this.spriteFont.MeasureString(this.strokeText).X + 20;
        }

        protected override void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.DebugChanged += EventDispatcher_HUDChanged;
            base.RegisterForEventHandling(eventDispatcher);
        }

        private void EventDispatcher_HUDChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnToggleHud)
            {
                if (this.StatusType == StatusType.Off)
                    this.StatusType = StatusType.Drawn | StatusType.Update;
                else
                    this.StatusType = StatusType.Off;
            }
        }
        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start game event
            if (eventData.EventType == EventActionType.OnStart)
            {
                //turn on update and draw i.e. hide the menu
                this.StatusType = StatusType.Update | StatusType.Drawn;
            }
            //did the event come from the main menu and is it a start game event
            else if (eventData.EventType == EventActionType.OnPause)
            {
                //turn off update and draw i.e. show the menu since the game is paused
                this.StatusType = StatusType.Off;
            }
        }
        protected override void ApplyDraw(GameTime gameTime)
        {
            this.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.Default, null);
            if (this.screenManager.ScreenType == ScreenUtility.ScreenType.SingleScreen)
            {
                DrawHudInfo(this.cameraManager.ActiveCamera);
            }
            else
            {
                foreach (Camera3D camera in cameraManager)
                    DrawHudInfo(camera);
            }
            this.spriteBatch.End();
        }

        public void updateStrokeCount()
        {
            strokeCounter += 1;
            this.strokeText = new StringBuilder("Strokes: " + this.strokeCounter);
        }

        public void updatePowerLevelOne(float power)
        {
            powerValue = power;
        }

        public void updateWin()
        {
            this.win = true;
        }

        private void DrawHudInfo(Camera3D camera)
        {
            this.textPosition = new Vector2(camera.Viewport.X, camera.Viewport.Y) + this.hudPosition;
            this.spriteBatch.DrawString(this.spriteFont, this.strokeText, this.textPosition, this.textColor);

            this.textPosition.X += this.textWidth;
            this.spriteBatch.DrawString(this.spriteFont, "Power: " + powerValue, this.textPosition, this.textColor);

            if (win)
            {
                this.textPosition.X -= this.textWidth * 2;
                this.textPosition.Y -= this.textWidth;
                this.spriteBatch.DrawString(this.spriteFont, "You reached Pluto!", this.textPosition, this.textColor);
            }
        }
    }
}