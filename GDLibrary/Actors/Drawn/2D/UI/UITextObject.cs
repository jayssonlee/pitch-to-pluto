﻿/*Represent text drawn in a 2D menu or UI element */
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Drawn._2D.UI
{
    public class UITextObject : UIObject
    {
        #region Variables
        private string text;
        private SpriteFont spriteFont;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = (value.Length >=0) ? value : "Default";
            }
        }

        public SpriteFont SpriteFont
        {
            get
            {
                return this.spriteFont;
            }

            set
            {
                this.spriteFont = value;
            }
        }
        #endregion
        public UITextObject(string id, ActorType actorType, StatusType statusType, Transform2D transform, 
            ColorParameters colorParameters, SpriteEffects spriteEffects, float layerDepth, string text, SpriteFont spriteFont)
            :base(id, actorType, statusType, transform, colorParameters, spriteEffects, layerDepth)
        {
            this.spriteFont = spriteFont;
            this.text = text;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(this.spriteFont, this.text, this.Transform.Translation, this.ColorParameters.Color,
                MathHelper.ToRadians(this.Transform.RotationInDegrees), this.Transform.Origin, this.Transform.Scale,
                this.SpriteEffects, this.LayerDepth);
        }

        public override bool Equals(object obj)
        {
            UITextObject other = obj as UITextObject;

            if(other == null)
            {
                return false;
            }
            else if(this == other)
            {
                return true;
            }

            return this.text.Equals(other.Text) && this.spriteFont.Equals(other.SpriteFont)
                && base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.text.GetHashCode();
            hash = hash * 17 + this.spriteFont.GetHashCode();
            hash = hash * 7 + base.GetHashCode();
            return hash;
        }

        public new object Clone()
        {
            IActor actor = new UITextObject("Clone - " + ID,
                this.ActorType,
                this.StatusType,
                (Transform2D)this.Transform.Clone(),
                (ColorParameters)this.ColorParameters.Clone(),
                this.SpriteEffects,
                this.LayerDepth,
                this.text,
                this.spriteFont); //shallow copy

            //clone each of the behaviour controllers
            if(this.ControllerList != null)
            {
                foreach(IController controller in this.ControllerList)
                {
                    actor.AttachController((IController)controller.Clone());
                }
            }
            return actor;
        }

        public override bool Remove()
        {
            this.text = null;
            return base.Remove();
        }
    }
}
