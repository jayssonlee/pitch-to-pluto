﻿/* A simple static camera in our 3D world to which we will later attach controllers*/
using GDLibrary.Enums;
using GDLibrary.Parameters.Camera;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors.Camera
{
    public class Camera3D : Actor3D
    {
        #region Variables
        private ProjectionParameters projectionParameters;
        private Viewport viewport;
        private Vector2 viewportCentre;
        private int drawDepth;
        #endregion
        #region Properties
        public Matrix View
        {
            get
            {
                return Matrix.CreateLookAt(this.Transform3D.Translation,
                    this.Transform3D.Translation + this.Transform3D.Look, this.Transform3D.Up);
            }
        }

        public Matrix Projection
        {
            get
            {
                return this.projectionParameters.Projection;
            }
        }

        public ProjectionParameters ProjectionParameters
        {
            get
            {
                return this.projectionParameters;
            }

            set
            {
                this.projectionParameters = value;
            }
        }

        public Viewport Viewport
        {
            get
            {
                return this.viewport;
            }

            set
            {
                this.viewport = value;
                this.viewportCentre = new Vector2(this.viewport.Width / 2.0f, this.viewport.Height / 2.0f);
            }
        }

        public Vector2 ViewportCentre
        {
            get
            {
                return this.viewportCentre;
            }
        }

        public int DrawDepth
        {
            get
            {
                return this.drawDepth;
            }

            set
            {
                this.drawDepth = value;
            }
        }

        public BoundingFrustum BoundingFrustum
        {
            get
            {
                return new BoundingFrustum(this.View * this.projectionParameters.Projection);
            }
        }
        #endregion

        public Camera3D(string id, ActorType actorType, Transform3D transform,
            ProjectionParameters projectionParameters, Viewport viewPort, int drawDepth,
            StatusType statusType) : base(id, actorType, transform, statusType)
        {
            this.projectionParameters = projectionParameters;
            this.viewport = viewPort;
            this.drawDepth = drawDepth;
        }

        //create a default 3D camera that we can clone
        public Camera3D(string id, ActorType actorType, Viewport viewPort)
            : this(id, actorType, Transform3D.Zero, ProjectionParameters.StandardMediumFourThree,
                  viewPort, 0, StatusType.Update)
        {

        }

        public override bool Equals(object obj)
        {
            Camera3D other = obj as Camera3D;
            return Vector3.Equals(this.Transform3D.Translation, other.Transform3D.Translation)
                && Vector3.Equals(this.Transform3D.Look, other.Transform3D.Look)
                && Vector3.Equals(this.Transform3D.Up, other.Transform3D.Up)
                && this.ProjectionParameters.Equals(other.ProjectionParameters);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.Transform3D.Translation.GetHashCode();
            hash = hash * 17 + this.Transform3D.Look.GetHashCode();
            hash = hash * 13 + this.Transform3D.Up.GetHashCode();
            return hash;
        }

        public new object Clone()
        {
            return new Camera3D("Clone " + this.ID,
                this.ActorType, (Transform3D)this.Transform3D.Clone(),
                (ProjectionParameters)this.projectionParameters.Clone(),
                this.Viewport, 0, StatusType.Update);
        }

        public override string ToString()
        {
            return this.ID + "Translation: " + MathUtility.Round(this.Transform3D.Translation, 0)
            + ", Look: " + MathUtility.Round(this.Transform3D.Look, 0)
            + ", Up: " + MathUtility.Round(this.Transform3D.Up, 0);
        }
    }
}
